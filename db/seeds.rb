# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Page.create(url: "http://paulsmith.com", tags: "Paul Smith")
Page.create(url: "http://ryanmackergie.com", tags: "Ryan Mackergie")
Page.create(url: "http://iantusil.com", tags: "Ian Tusil")
Page.create(url: "http://paulmcjoy.com", tags: "Paul Mcjoy")