class Page < ActiveRecord::Base

  def self.search(fname, lname)
    search = "#{fname} #{lname}"
    (!fname and !lname) ? [] : Page.where((['pages.tags LIKE ?'] * search.split.length).join(' OR '), *search.split(" ").map{|keyword| "%#{keyword} %"})
  end
end
