class PagesController < ApplicationController

  def show
    # Rails.logger.debug params[:q].split(" ").join("% %")
    @pages = Page.search params[:fname], params[:lname]
    Rails.logger.debug @pages.inspect
    if @pages.empty?
      Search.create(fname: params[:fname], lname: params[:lname])
    end
  end
end
