class SearchesController < ApplicationController
  
  def index
    @searches = Search.select('*, count(lname) as lcount, count(fname) as fcount').group(['fname', 'lname']).order('fcount desc, lcount desc').order('lcount desc')
  end
end
