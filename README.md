Installation
==

Pretty standard rails installation with `rake db:create` `rake db:migrate` stuff:

* but also run `rake db:seed` afterwards

Try
==


Try search in browser while app is running:

* Search for `paul` or `ian`

or from the list below

```
Paul Smith
Ryan Mackergie
Ian Tusil
Paul Mcjoy
```