Rails.application.routes.draw do
  get 'search' => 'pages#show'
  root 'pages#show'
  resources :searches
end